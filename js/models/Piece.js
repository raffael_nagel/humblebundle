var Piece = function(type, color, x , y){
    this.type = type;
    this.color = color;
    this.first_move = true;
    this.x = x;
    this.y = y;
};

Piece.prototype = {
    getAllowedMoves: function(){
        var moves = [];
        switch(this.type){
            case 'king'   :
                moves = [{x : 0, y : 1},{x : 0, y : -1},
                    {x : 1, y : 0},{x : -1, y : 0},
                    {x : 1, y : 1},{x : -1, y : -1},
                    {x : 1, y : -1},{x : -1, y : 1}];
                break;
            case 'queen'  :
                moves = [{x : 0, y : 1},{x : 0, y : -1},
                    {x : 1, y : 0},{x : -1, y : 0},
                    {x : 1, y : 1},{x : -1, y : -1},
                    {x : 1, y : -1},{x : -1, y : 1}];
                break;
            case 'rook'   :
                moves = [{x : 0, y : 1}, {x : 0, y : -1},
                    {x : 1, y : 0}, {x : -1, y : 0}];
                break;
            case 'bishop' :
                moves = [{x : 1, y : 1}, {x : -1, y : -1},
                    {x : 1, y : -1}, {x : -1, y : 1}];
                break;
            case 'knight' :
                moves = [{x : 2, y : 1}, {x : 2, y : -1},
                    {x : -2, y : 1}, {x : -2, y : -1},
                    {x : 1, y : 2}, {x : -1, y : 2},
                    {x : 1, y : -2}, {x : -1, y : -2}];
                break;
            case 'pawn'   :
                if(this.color == "white") {
                    moves = [{x: 1, y: 0}];
                }else{
                    moves = [{x: -1, y: 0}];
                }
                break;
            default       : break;
        }
        return moves;
    },

    getAllowedSteps: function(){
        if(this.type == "king" || this.type == "knight" || this.type == "pawn") return 1;
        return "infinite";
    }
};

Piece.getSymbol = function(piece){
    switch(piece.type){
        case 'king'   :
            return {white:"&#9812;", black:"&#9818;"};
        case 'queen'  :
            return {white:"&#9813;", black:"&#9819;"};
        case 'rook'   :
            return {white:"&#9814;", black:"&#9820;"};
        case 'bishop' :
            return {white:"&#9815;", black:"&#9821;"};
        case 'knight' :
            return {white:"&#9816;", black:"&#9822;"};
        case 'pawn'   :
            return {white:"&#9817;", black:"&#9823;"};
        default       :
            return '_';
    }
};