var Board = function(size){
    this.size = size;
    this.positions = null;
    this.init();
};

Board.prototype = {

    init: function () {
        this.positions = [];
        for(var i = 0; i < this.size; i++) {
            this.positions[i] = [];
            for(var j = 0; j < this.size; j++) {
                this.positions[i][j] = undefined;
            }
        }
    },

    getPieceAt: function(x , y){
        if(x < this.positions.length && y < this.positions[0].length) {
            return this.positions[x][y];
        }else{
            return null;
        }
    },

    addPiece: function(piece){
        this.positions[piece.x][piece.y] = piece;
    },

    addPieces: function(pieces){
        for(id in pieces){
            var p = pieces[id];
            this.positions[p.x][p.y] = p;
        }
    },

    movePiece: function(x, y, newX, newY){
        var p = this.positions[x][y];
        if(p == undefined)return;

        p.x = newX;
        p.y = newY;
        p.first_move = false;

        this.positions[x][y] = undefined;
        this.positions[newX][newY] = p;
    },

    checkPieceCollision: function(piece, x , y){
        if(x > this.positions.length - 1 || y > this.positions[0].length - 1)return true;
        if(x < 0 || y < 0)return true;
        if(this.positions[x][y] == undefined)return false;
        if(piece.color == this.positions[x][y].color)return true;
        return false;
    },

    checkPieceCapture: function(piece, x , y){
        if(this.positions[x][y] == undefined)return false;
        if(piece.color != this.positions[x][y].color)return true;
        return false;
    },

    getPawnMoves: function (pawn) {
        var possibleMoves = [];
        var newX = pawn.x;
        var newY = pawn.y;

        var step;
        if(pawn.color == "white"){
            step = -1;
        }else{
            step = 1;
        }

        newX = newX + step;
        if(this.positions[newX][newY] == undefined){
            possibleMoves.push({x: newX, y: newY});
            if(pawn.first_move){
                newX = newX + step;
                if(this.positions[newX][newY] == undefined){
                    possibleMoves.push({x: newX, y: newY});
                }
            }
        }

        if (pawn.x + step > 0 && pawn.x + step < this.positions.length &&
            pawn.y + 1 < this.positions[0].length &&
            this.positions[pawn.x + step][pawn.y + 1] != undefined &&
            this.positions[pawn.x + step][pawn.y + 1].color != pawn.color){
            possibleMoves.push({x: pawn.x+step, y: pawn.y+1});
        }
        if (pawn.x + step > 0 && pawn.x + step < this.positions.length &&
            pawn.y - 1 > 0 &&
            this.positions[pawn.x + step][pawn.y - 1] != undefined &&
            this.positions[pawn.x + step][pawn.y - 1].color != pawn.color){
            possibleMoves.push({x: pawn.x+step, y: pawn.y-1});
        }

        return possibleMoves;
    },

    getPieceMoves: function(x , y){
        var piece = this.positions[x][y];
        if(piece == undefined)return;
        if(piece.type == "pawn") return this.getPawnMoves(piece);
        var moves = piece.getAllowedMoves();

        var possibleMoves = [];
        for(m in moves){
            var newX = piece.x;
            var newY = piece.y;

            if(piece.getAllowedSteps(piece.type) == 1){
                newX = newX + moves[m].x;
                newY = newY + moves[m].y;
                if(!this.checkPieceCollision(piece, newX, newY)){
                    possibleMoves.push({x: newX, y: newY});
                }
            }else {
                var collision = 0;
                while (newX < this.positions.length && newY < this.positions[0].length && newX > -1 && newY > -1 && collision == 0) {
                    newX = newX + moves[m].x;
                    newY = newY + moves[m].y;
                    if(this.checkPieceCollision(piece, newX, newY)){
                        collision = 1;
                    }else {
                        possibleMoves.push({x: newX, y: newY});
                        if(this.checkPieceCapture(piece, newX, newY)){
                            collision = 1;
                        }
                    }
                }
            }
        }
        return possibleMoves;
    },

    getPlayerMoves: function(player){
        for(var i = 0; i < this.positions.length; i++){
            for(var j = 0; j < this.positions[0].length; j++){
                if(this.positions[i][j] != undefined){
                    if(this.positions[i][j].color == player) {
                        console.log(chessBoard.positions[i][j].type + "(" + i + ", " + j + ") can move to: ");
                        var moves = chessBoard.getPieceMoves(i, j);
                        for (m in moves) {
                            console.log("(" + moves[m].x + ", " + moves[m].y + ")");
                        }
                    }
                }
            }
        }
    },

    print: function(){
        if(this.positions == [])return;

        for(var i = 0; i < this.size; i++) {
            var line = "[" + i + "] ";
            for(var j = 0; j < this.size; j++) {
                if(this.positions[i][j] == undefined){
                    line = line + "|" + "_";
                }else{
                    if(this.positions[i][j] == "white"){
                        line = line + "|" + Piece.getSymbol(this.positions[i][j]).white;
                    }else{
                        line = line + "|" + Piece.getSymbol(this.positions[i][j]).black;
                    }
                }
            }
            console.log(line + "|");
        }
    }

};