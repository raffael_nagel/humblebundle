var boardView = function(){
    this.$board = $('.chess-board');
};

boardView.prototype = {

    printPieces: function (pieces) {
        for(id in pieces){
            var p = pieces[id];
            if(p.color == "white") {
                $('#tile-' + p.x + p.y).html(Piece.getSymbol(p).white);
            }else{
                $('#tile-' + p.x + p.y).html(Piece.getSymbol(p).black);
            }
        }
    },

    clearPaths: function(){
        $("tr td:contains('+')").each(function(){
            $(this).html('');
        });
        $('.tile').attr("style","");
        $('.tile').data("allowed-move",false);
    },

    printConsole: function(board){
        if(board.positions == [])return;

        for(var i = 0; i < board.size; i++) {
            var line = "[" + i + "] ";
            for(var j = 0; j < board.size; j++) {
                if(board.positions[i][j] == undefined){
                    line = line + "|" + "_";
                }else{
                    if(board.positions[i][j] == "white"){
                        line = line + "|" + Piece.getSymbol(board.positions[i][j]).white;
                    }else{
                        line = line + "|" + Piece.getSymbol(board.positions[i][j]).black;
                    }
                }
            }
            console.log(line + "|");
        }
    }
};