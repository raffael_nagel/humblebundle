
var player1 = "white";
var player2 = "black";
var player1_pieces = [];
var player2_pieces = [];

var chessBoard = new Board(8);

//white pieces
player1_pieces.push(new Piece("pawn",  player1, 6, 4));
player1_pieces.push(new Piece("pawn",  player1, 6, 2));
player2_pieces.push(new Piece("rook",  player1, 5, 7));
player2_pieces.push(new Piece("bishop",player1, 6, 0));
player2_pieces.push(new Piece("bishop",player1, 3, 4));
player2_pieces.push(new Piece("queen", player1, 7, 4));
player2_pieces.push(new Piece("king",  player1, 7, 3));
player2_pieces.push(new Piece("knight",  player1, 7, 6));

//Black pieces
var bPawn1 = new Piece("pawn",  player2, 2, 3);
bPawn1.first_move = false;
player2_pieces.push(bPawn1);
player2_pieces.push(new Piece("rook",  player2, 0, 0));
player2_pieces.push(new Piece("king",  player2, 0, 4));
player2_pieces.push(new Piece("queen", player2, 3, 1));
player2_pieces.push(new Piece("knight",player2, 2, 1));
player2_pieces.push(new Piece("bishop",player2, 6, 1));
player2_pieces.push(new Piece("pawn",player2, 1, 6));


chessBoard.addPieces(player1_pieces);
chessBoard.addPieces(player2_pieces);

//print pieces
var boardView = new boardView();
boardView.printPieces(player1_pieces);
boardView.printPieces(player2_pieces);


//Selected Piece
var selectedPiece = null;
var playerTurn = "white";

//OnClick event
$('.tile').click(function () {
    //get piece from board
    var piece = chessBoard.getPieceAt($(this).data("x"), $(this).data("y"));

    //check player's turn
    if(piece != undefined && piece.color != playerTurn && selectedPiece == null)return;

    //check if player is selecting a piece to move

    if(selectedPiece == null || (selectedPiece != null && piece != undefined && selectedPiece.color == piece.color)) {
        selectedPiece = piece;
    }else{

        if($(this).data("allowed-move")){
            $('#tile-' + selectedPiece.x + selectedPiece.y).html('');

            chessBoard.movePiece(selectedPiece.x, selectedPiece.y, $(this).data("x"), $(this).data("y"));
            if(selectedPiece.color == "white") {
                $(this).html(Piece.getSymbol(selectedPiece).white);
            }else{
                $(this).html(Piece.getSymbol(selectedPiece).black);
            }

            boardView.clearPaths();
            selectedPiece = null;

            //end turn
            if(playerTurn == "white"){
                playerTurn = "black";
            }else{
                playerTurn = "white";
            }

            return;
        }
    }

    //check player's turn
    if(piece != undefined && piece.color != playerTurn)return;

    boardView.clearPaths();
    //show allowed moves
    if($(this).html() != ''){
        var moves = chessBoard.getPieceMoves(piece.x, piece.y);
        if(moves == null)return;
        for(m in moves){
            if($('#tile-' + moves[m].x + moves[m].y).html() != ""){
                if(chessBoard.checkPieceCapture(piece, moves[m].x, moves[m].y)){
                    $('#tile-'+moves[m].x + moves[m].y).attr("style","color: red");
                }
            }else {
                $('#tile-' + moves[m].x + moves[m].y).html('+');
            }
            $('#tile-' + moves[m].x + moves[m].y).data("allowed-move",true);
        }
    }
});
