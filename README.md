#HumbleBundle Challenge
####Author: Raffael.nagel@gmail.com

#Challenge

Please write a program that takes these inputs and produces these outputs:

##Inputs

● The current configuration of a chess board (which pieces are where)

● Which player's turn it is to make the next move 

##Outputs

● A list of all moves the current player can legally make this turn

###IMPORTANT:
To simplify the task, you don't need to worry about whether a move would lead to the player's King being in check. Similarly, you are welcome to ignore the "castling" and "en passant" rules. ○ For details on legal chess moves, see the "Basic moves" section here under "Gameplay": https://en.wikipedia.org/wiki/Rules_of_chess

#Solution
The solution for this challenge was developed using Javascript and HTML.
The result is a webpage with a chess board where the user can click on the pieces to see the allowed movements.
Also, to view a list of all movements that a player can perform you can run the command "chessBoard.getPlayerMoves(player_color)" (replacing player_color with "white" or "black" ) on the browser console.

###live version
####http://www.raffael.nagel.link/humblebundle